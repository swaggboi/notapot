#!/usr/bin/env ruby
# coding: utf-8

# Daniel Bowling <swaggboi@slackware.uk>
# Nov 2020

# TODO: The "Password:" prompt should NOT echo back however it does
# because I failed to implement the IAC exchange correctly... The
# phoney login attempts I've seen are certainly automated thus I don't
# think anyone would notice such detail but after some tests I thought
# I had it right but then I got different results (login hangs after
# entering password) by changing the listening port number which makes
# me think the times it did work was probably a lucky bug. I'd like to
# come back to this and make it convincing enough to fool a human
# See:
# https://tools.ietf.org/html/rfc857
# See++:
# https://tools.ietf.org/html/rfc1143
# Grabbed squence for telnet negotiation from this CPAN module:
# https://metacpan.org/pod/Net::TCP::PtyServer

require 'yaml'
require 'socket'
require 'mysql'

cmd    = File.basename($PROGRAM_NAME)
port   = nil
conf   = nil
server = nil
my     = nil
stmt   = nil

# Grab the conf file
%w[yaml yml].each do |ext|
  conf =
    if File.file?("#{ENV['HOME']}/.notapot.#{ext}")
      YAML.load_file("#{ENV['HOME']}/.notapot.#{ext}")
    elsif File.file?(".notapot.#{ext}")
      YAML.load_file(".notapot.#{ext}")
    end
end

# Set the listening port
port   = conf['port'] || 23
# Create the TCP server object
server = TCPServer.new(port)

# Create MySQL object
my = Mysql.connect(
  conf['hostname'],
  conf['username'],
  conf['password'],
  conf['dbname']
)

# Create table if it doesn't exist
my.query(
  'CREATE TABLE IF NOT EXISTS logins (
  login_id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  created_at TIMESTAMP
  DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  sock_domain VARCHAR(32),
  remote_port INT UNSIGNED,
  remote_hostname VARCHAR(128),
  remote_ip VARCHAR(64),
  username VARCHAR(128),
  password VARCHAR(128)
  )'
)

puts "#{cmd}: Awaiting TCP connections on port #{port}..."

loop do
  # Use Thread to better handle multiple clients
  Thread.start(server.accept) do |client|
    sock_domain,
    remote_port,
    remote_hostname,
    remote_ip = client.peeraddr(:hostname)

    # Challenge for username/password
    client.print "#{Socket.gethostname} login: "
    # Kill the first 30 characters... Overflow from the IAC exchange
    # that needs to be handled correctly. Matching for unicode
    # characters breaks login; hands at "Password:" prompt
    username = client.gets.chomp.gsub(/.{30}/, '')
    client.print 'Password: '
    # This is broken 🤦 see paragraph at top
    #    # IAC DONT ECHO
    #    client.print 255.chr + 254.chr + 1.chr
    #    # IAC WILL ECHO
    #    client.print 255.chr + 251.chr + 1.chr
    #    # IAC DO SUPPRESS-GA
    #    client.print 255.chr + 253.chr + 3.chr
    #    # IAC WILL SUPPRESS-GA
    #    client.print 255.chr + 251.chr + 3.chr
    #    # IAC DONT LINEMODE
    #    client.print 255.chr + 254.chr + 34.chr
    #    # IAC WONT LINEMODE
    #    client.print 255.chr + 252.chr + 34.chr
    #    # IAC DO NAWS
    #    client.print 255.chr + 253.chr + 31.chr
    #    password = client.gets.chomp.gsub(/^../, '')
    password = client.gets.chomp
    client.close

    # Write to DB (prepare statement first per best practices)
    stmt = my.prepare(
      'INSERT INTO logins (
              sock_domain,
              remote_port,
              remote_hostname,
              remote_ip,
              username,
              password
       )
       VALUES (?, ?, ?, ?, ?, ?)'
    )
    stmt.execute(
      sock_domain,
      remote_port,
      remote_hostname,
      remote_ip,
      username,
      password
    )

    # Print output
    remote_host = remote_hostname.nil? ? remote_ip : remote_hostname
    puts "#{cmd}: #{username}@#{remote_host}:#{remote_port} supplied " +
         "password #{password}"
  end
end
